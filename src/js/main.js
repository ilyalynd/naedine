import Swiper from 'swiper';
import { Thumbs } from 'swiper/modules';

// Получает Cookie
function getCookie(name) {
  let matches = document.cookie.match(new RegExp('(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'));

  return matches ? decodeURIComponent(matches[1]) : undefined;
}

(() => {
  const html = document.querySelector('html'),
        popup = document.getElementById('popup'),
        menu = document.getElementById('menu');


  // Добавляет слайдер для галереи изображений
  const productThumbs = new Swiper('.product-thumbs', {
    slidesPerView: 5,
    spaceBetween: 4,
    freeMode: true,
    watchSlidesProgress: true,
    breakpoints: {
      1280: {
        slidesPerView: 10,
        spaceBetween: 8
      }
    }
  });
  const productGallery = new Swiper('.product-gallery', {
    modules: [ Thumbs ],
    thumbs: {
      swiper: productThumbs
    }
  });


  // Открывает popup-окно
  let linkOpenPopup = document.querySelectorAll('[data-action=open-popup]');

  const openPopup = (event, module) => {
    event.preventDefault();

    if (module === undefined) {
      module = event.currentTarget.getAttribute('data-popup');
    }

    popup.classList.add('open');
    document.querySelector('.' + module).classList.add('open');
    html.style.overflowY = 'hidden';
  }

  linkOpenPopup.forEach(link => link.addEventListener('click', openPopup));


  // Закрывает popup-окно
  let linkClosePopup = document.querySelectorAll('[data-action=close-popup]');

  const closePopup = (event) => {
    event.preventDefault();

    let modules = document.querySelectorAll('.popup__module');

    modules.forEach(module => {
      if (module.classList.contains('open') && !module.classList.contains('user-block')) {
        module.classList.remove('open');
        popup.classList.remove('open');
        html.style.overflowY = 'visible';
      }
    });
  }

  linkClosePopup.forEach(link => link.addEventListener('click', closePopup));


  // Открывает popup-окно c проверкой пользователя на возраст
  if (getCookie('age-verify') === undefined) {
    window.addEventListener('load', (event) => {
      setTimeout(() => openPopup(event, 'age-verify'), 4000);
    });
  }


  // Отключает проверку пользователя на возраст
  let linkYes = document.querySelectorAll('[data-action=yes]');

  const responseYes = (event) => {
    event.preventDefault();

    closePopup(event);
    document.cookie = 'age-verify=true;max-age=' + 60*60*24*30 + ';'; // ..на месяц
  }

  linkYes.forEach(link => link.addEventListener('click', responseYes));


  // Блокирует доступ пользователя к сайту
  let linkNo = document.querySelectorAll('[data-action=no]');

  const responseNo = (event) => {
    event.preventDefault();

    closePopup(event);
    openPopup(event, 'user-block');
  }

  linkNo.forEach(link => link.addEventListener('click', responseNo));


  // Открывает фильтры
  let linkOpenFilter = document.querySelectorAll('[data-action=open-filter]');

  const openFilter = (event) => {
    event.preventDefault();

    // TODO
  }

  linkOpenFilter.forEach(link => link.addEventListener('click', openFilter));


  // Закрывает фильтры
  let linkCloseFilter = document.querySelectorAll('[data-action=сlose-filter]');

  const closeFilter = (event) => {
    event.preventDefault();

    // TODO
  }

  linkCloseFilter.forEach(link => link.addEventListener('click', closeFilter));


  // Очищает все фильтры
  let linkClearFilter = document.querySelectorAll('[data-action=clear-filter]');

  const clearFilter = (event) => {
    event.preventDefault();

    let allСheckBoxes = document.querySelectorAll('.checkbox');

    allСheckBoxes.forEach(checkBox => checkBox.classList.remove('checked'));
    linkToggleBox.forEach(link => link.closest('.checkbox').classList.remove('open'));
    toggleSelect(event);
  }

  linkClearFilter.forEach(link => link.addEventListener('click', clearFilter));


  // Открывает меню
  let linkOpenMenu = document.querySelectorAll('[data-action=open-menu]');

  const openMenu = (event) => {
    event.preventDefault();

    // Прокрутка к началу страницы, чтобы выровнять меню
    window.scroll({
      top: 0,
      behavior: 'auto'
    });

    menu.classList.add('open');
    html.style.overflowY = 'hidden';

    event.stopPropagation();
  }

  linkOpenMenu.forEach(link => link.addEventListener('click', openMenu));


  // Закрывает меню
  let linkCloseMenu = document.querySelectorAll('[data-action=close-menu]');

  const closeMenu = (event) => {
    event.preventDefault();

    menu.classList.remove('open');
    html.style.overflowY = 'visible';

    event.stopPropagation();
  }

  linkCloseMenu.forEach(link => link.addEventListener('click', closeMenu));


  // Показывает / скрывает опции селекта
  let linkToggleSelect = document.querySelectorAll('[data-action=toggle-select]');

  const toggleSelect = (event) => {
    event.preventDefault();

    let allSelect = document.querySelectorAll('.select'),
        currentSelect = event.currentTarget.closest('.select');

    if (!currentSelect.classList.contains('open')) {
      allSelect.forEach(select => select.classList.remove('open'));
      currentSelect.classList.add('open');
    } else {
      currentSelect.classList.remove('open');
    }
  }

  linkToggleSelect.forEach(link => link.addEventListener('click', toggleSelect));


  // Выбирает чекбоксы
  let allCheckBoxes = document.querySelectorAll('[data-action=check]');

  const choiceCheckBox = (event) => {
    event.preventDefault();

    let checkBox = event.currentTarget.closest('.checkbox'),
        selectOptionСhildren = checkBox.nextElementSibling,
        selectOptionParent = checkBox.closest('.select-options__inner'),
        childrenCheckBoxes;

    if (!checkBox.classList.contains('checked')) {
      checkBox.classList.add('checked');

      // Проверяет дочерние чекбоксы
      if (selectOptionСhildren !== null) {
        childrenCheckBoxes = selectOptionСhildren.querySelectorAll('.checkbox');

        childrenCheckBoxes.forEach(checkBox => checkBox.classList.add('checked'));
      }

      // Проверяет родительский и соседние чекбоксы
      if (selectOptionParent !== null) {
        childrenCheckBoxes = selectOptionParent.querySelectorAll('.checkbox');

        let count = 0;

        childrenCheckBoxes.forEach(checkBox => {
          if (!checkBox.classList.contains('checked')) {
            count++;
          }
        });

        if (count == 0) {
          selectOptionParent.previousElementSibling.classList.add('checked');
        }
      }
    } else {
      checkBox.classList.remove('checked');

      // Проверяет дочерние чекбоксы
      if (selectOptionСhildren !== null) {
        childrenCheckBoxes = selectOptionСhildren.querySelectorAll('.checkbox');

        childrenCheckBoxes.forEach(checkBox => checkBox.classList.remove('checked'));
      }

      // Проверяет родительский чекбокс
      if (selectOptionParent !== null) {
        selectOptionParent.previousElementSibling.classList.remove('checked');
      }
    }
  }

  allCheckBoxes.forEach(checkBox => checkBox.addEventListener('click', choiceCheckBox));


  // Показывает / скрывает варианты
  let linkToggleBox = document.querySelectorAll('[data-action=toggle-box]');

  const toggleBox = (event) => {
    event.preventDefault();

    let allСheckBoxes = document.querySelectorAll('.checkbox'),
        currentСheckBox = event.currentTarget.closest('.checkbox');

    if (!currentСheckBox.classList.contains('open')) {
      allСheckBoxes.forEach(checkBox => checkBox.classList.remove('open'));
      currentСheckBox.classList.add('open');
    } else {
      currentСheckBox.classList.remove('open');
    }
  }

  linkToggleBox.forEach(link => link.addEventListener('click', toggleBox));


  // Выбирает варианты доставки / оплаты
  let allRadio = document.querySelectorAll('.radio');

  function choiceRadio() {
    let currentRadio = this.closest('.form__group').querySelectorAll('.radio'),
        input = this.querySelector('input[type=radio]');

    if (!this.classList.contains('disabled')) {
      for (let i = 0; i < currentRadio.length; i++) {
        currentRadio[i].classList.remove('checked');
        currentRadio[i].querySelector('input[type=radio]').checked = false;
      }

      if (!this.classList.contains('checked')) {
        this.classList.add('checked');
        input.checked = true;
      }
    }
  }

  allRadio.forEach(radio => radio.addEventListener('click', choiceRadio));


  // Добавляет раскрывающийся список с характеристиками товаров
  let allParameters = document.querySelectorAll('.product-list__parameter');

  function toggleParameters() {
    let parameterToggle = this.getAttribute('area-expanded'),
        icon = this.querySelector('.product-list__icon svg use');

    for (let i = 0; i < allParameters.length; i++) {
      allParameters[i].setAttribute('area-expanded', 'false');
      allParameters[i].querySelector('.product-list__icon svg use').setAttribute('href', '/image/icon/plus.svg#plus');
    }

    if (parameterToggle == 'false') {
      this.setAttribute('area-expanded', 'true');
      icon.setAttribute('href', '/image/icon/close.svg#close');
    }
  }

  allParameters.forEach(parameter => parameter.addEventListener('click', toggleParameters));


  // Добавляет раскрывающийся список FAQ
  let allQuestions = document.querySelectorAll('.faq-list__question');

  function toggleQuestions() {
    let questionToggle = this.getAttribute('area-expanded'),
        icon = this.querySelector('.faq-list__icon svg use');

    for (let i = 0; i < allQuestions.length; i++) {
      allQuestions[i].setAttribute('area-expanded', 'false');
      allQuestions[i].querySelector('.faq-list__icon svg use').setAttribute('href', '/image/icon/plus.svg#plus');
    }

    if (questionToggle == 'false') {
      this.setAttribute('area-expanded', 'true');
      icon.setAttribute('href', '/image/icon/close.svg#close');
    }
  }

  allQuestions.forEach(question => question.addEventListener('click', toggleQuestions));


  // Манипуляции с корзиной и товарами
  let cart = [],
      wishlist = [],
      currentCount = 1;

  // Уменьшает количество товаров на странице товара
  let linkMinusProduct = document.querySelectorAll('[data-action=minus-product]');

  const minusProduct = (event) => {
    event.preventDefault();

    let counter = document.querySelector('.product-control-counter span');

    if (currentCount > 1) {
      currentCount--;
      counter.textContent = currentCount;
    }
  }

  linkMinusProduct.forEach(link => link.addEventListener('click', minusProduct));


  // Увеличивает количество товаров на странице товара
  let linkPlusProduct = document.querySelectorAll('[data-action=plus-product]');

  const plusProduct = (event) => {
    event.preventDefault();

    let counter = document.querySelector('.product-control-counter span');

    currentCount++;
    counter.textContent = currentCount;
  }

  linkPlusProduct.forEach(link => link.addEventListener('click', plusProduct));


  // Добавляет товар в корзину
  let linkAddCart = document.querySelectorAll('[data-action=add-cart]');

  const addCart = (event) => {
    event.preventDefault();

    let productId = Number(event.currentTarget.getAttribute('data-product-id')),
        productIndex = cart.findIndex(({ id }) => id == productId),
        newProduct = {
          id: productId,
          count: null
        };

    if (productIndex !== -1) {
      newProduct.count = currentCount + cart[productIndex].count;
      cart.splice(productIndex, 1, newProduct);
    } else {
      newProduct.count = currentCount;
      cart.push(newProduct);
    }

    updateCountCart();
  }

  linkAddCart.forEach(link => link.addEventListener('click', addCart));


  // Обновляет счётчик товаров в корзине
  const updateCountCart = () => {
    let counter = document.querySelector('.header-cart a span'),
        countCart = cart.length;

    if (countCart > 0) {
      counter.classList.add('show');
    } else {
      counter.classList.remove('show');
    }

    counter.textContent = countCart;
  }

  updateCountCart();


  // Добавляет товар в вишлист
  let linkAddWishlist = document.querySelectorAll('[data-action=add-wishlist]');

  const addWishlist = (event) => {
    event.preventDefault();

    let productId = Number(event.currentTarget.getAttribute('data-product-id')),
        productIndex = wishlist.findIndex(({ id }) => id == productId),
        newProduct = {
          id: productId,
          count: null
        };

    if (productIndex !== -1) {
      newProduct.count = currentCount + wishlist[productIndex].count;
      wishlist.splice(productIndex, 1, newProduct);
    } else {
      newProduct.count = currentCount;
      wishlist.push(newProduct);
    }
  }

  linkAddWishlist.forEach(link => link.addEventListener('click', addWishlist));


  // Удаляет товар из корзины
  let linkRemoveCart = document.querySelectorAll('[data-action=remove-cart]');

  const removeCart = (event) => {
    event.preventDefault();

    let productId = event.currentTarget.getAttribute('data-product-id'),
        productIndex = cart.findIndex(({ id }) => id == productId);

    cart.splice(productIndex, 1);
    updateCountCart();
  }

  linkRemoveCart.forEach(link => link.addEventListener('click', removeCart));


  // Удаляет товар из вишлиста
  let linkRemoveWishlist = document.querySelectorAll('[data-action=remove-wishlist]');

  const removeWishlist = (event) => {
    event.preventDefault();

    let productId = event.currentTarget.getAttribute('data-product-id'),
        productIndex = wishlist.findIndex(({ id }) => id == productId);

    wishlist.splice(productIndex, 1);
  }

  linkRemoveWishlist.forEach(link => link.addEventListener('click', removeWishlist));
})();


// js
import './js/main';

// css
import './assets/css/main.css';

// scss
import 'swiper/scss';
import 'swiper/scss/thumbs';
import './assets/scss/main.scss';

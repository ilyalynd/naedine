# Naedine change log

## Version 1.0.0 under development

- New: Added templates for all pages
- New: Added Manrope and Playfair Display fonts
- New: Added Swiper
- New: Added Pug, SCSS, PostCSS
- New: Added Webpack, Babel

## Project init October 21, 2023
